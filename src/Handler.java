import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;

public class Handler implements Runnable{
    static HashMap<String, Integer> optionCount = new HashMap<>();
    String sep = Character.toString ((char) 7);

    @Override
    public void run() {
        while (true){
            Socket[] sockets = new Socket[Server.clients.size()];
            sockets = Server.clients.toArray(sockets);

            System.out.println(Arrays.toString(Server.clients.toArray()));
            for(Socket socket : sockets){

                try {
                    PrintWriter writer = new PrintWriter(socket.getOutputStream());
                    BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    //Only reads if there is anything to read
                    if(reader.ready()){
                        String s = reader.readLine();

                        //save and return all the data, if the received data is not "". Then, only send back data.
                        if(!s.equals(sep)) {

                            if(optionCount.containsKey(s)) {
                                int i = optionCount.get(s);
                                optionCount.remove(s);
                                optionCount.put(s, i + 1);
                            }
                            else {
                                optionCount.put(s, 1);
                            }
                        }
                        optionCount.forEach((s1, integer) -> {
                            writer.write(s1 + ";" + integer + "\n");
                            writer.flush();
                        });

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    static HashMap update(){
        return optionCount;
    }
}

