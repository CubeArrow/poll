import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Client extends Application{
    String sep = Character.toString ((char) 7);
    String theme;
    ArrayList<String> options = new ArrayList<>();
    HashMap<String, Integer> optionCount = new HashMap<>();
    BorderPane optionPane;
    ObservableList<PieChart.Data> list = FXCollections.observableList(new ArrayList<PieChart.Data>());
    BufferedReader reader;
    PrintWriter writer;

    @Override
    public void start(Stage stage) {
        System.out.println("Start!");
        try {
            String host = input("localhost", "Connection", null, "Please enter the Host's id: ");
            String port = input("5555", "Connection", null, "Please enter the Host's Port: ");

            Socket socket = new Socket(host, Integer.parseInt(port));

            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new PrintWriter(socket.getOutputStream());

            //gets all the Options and the Theme
            theme = reader.readLine();
            System.out.println("Received: " + theme);
            Thread.sleep(5);
            while(reader.ready()){
                String s = reader.readLine();
                options.add(s);
                System.out.println("Received: " + s);
            }

            
            Label themelbl = new Label("Theme: " + theme);
            Button updatebtn = new Button("update");

            
            optionPane = new BorderPane();
            
            VBox optionBox = new VBox();
            optionBox.setAlignment(Pos.CENTER_LEFT);

            CheckBox[] checkBoxes = new CheckBox[options.size()];
            
            for(int i = 0; i < options.size(); i++){
                CheckBox checkBox = new CheckBox(options.get(i));
                
                checkBox.setSelected(false);
                
                checkBox.setOnAction((ActionEvent event) -> {

                    //Write the name of the Option to the Server, receive the coming data, save it and display it.
                    writer.write(checkBox.getText() + "\n");
                    writer.flush();
                    System.out.println("Wrote: " + checkBox.getText());
                    while(true) {
                        try {
                            if(reader.ready()) {
                                receiveData();
                                updateUI();
                                break;
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });

                //Save the checkbox in the Array of checkboxes.
                checkBoxes[i] = checkBox;
            }
            updatebtn.setOnAction(event -> {
                optionCount = new HashMap<>();
                writer.write(sep + "\n");
                writer.flush();
                System.out.println("Wrote: " + sep);
                while(true) {
                    try {
                        if(reader.ready()) {
                            receiveData();
                            updateUI();
                            break;
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            optionBox.getChildren().addAll(checkBoxes);

            optionPane.setTop(themelbl);
            optionPane.setCenter(optionBox);
            optionPane.setBottom(updatebtn);

            Scene scene = new Scene(optionPane, 500, 600);

            stage.setScene(scene);
            stage.show();

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }


    //receives data and adds it to the list for the Pie chart
    private void receiveData() {
        try{

            while (reader.ready()) {

                String[] s = reader.readLine().split(";");
                optionCount.put(s[0], Integer.valueOf(s[1]));
                System.out.println("Received: " + s[0] + ", " + s[1]);
            }

            }catch (Exception e) {
                e.printStackTrace();
            }
            list = FXCollections.observableList(new ArrayList<PieChart.Data>());
            for(Map.Entry<String, Integer> entry : optionCount.entrySet()) {
                list.add(new PieChart.Data(entry.getKey(), entry.getValue()));
            }
    }


    //Setting the center to a Pie chart
    private void updateUI() {
        System.out.println("updateUI!");
        list.forEach(data ->
                data.nameProperty().bind(
                        Bindings.concat(data.getName(), ": ", data.pieValueProperty().intValue())
                )
        );
        optionPane.setCenter(new Label());
    }



    public static void main(String[] args) {
        launch(args);
    }

    private String input(String defaultValue, String title, String headerText, String contentText) {
        TextInputDialog dialog = new TextInputDialog(defaultValue);
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentText);
        return dialog.showAndWait().get();
    }
}

