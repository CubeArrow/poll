import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server implements Runnable{
    String theme;
    ArrayList<String> options;
    static ArrayList<Socket> clients = new ArrayList<>();

    //Declaring all the Variables we need
    public Server(String theme, ArrayList<String> options){
        this.theme = theme;
        this.options = options;
    }

    //Starting a new Instance of the Server GUI
    public static void main(String[] args) {
        ServerGUI.start();
    }

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(5555);
            System.out.println("Started the Server");

            new Thread(new Handler()).start();
            while (true){
                Socket socket = serverSocket.accept();
                System.out.println("New Client");

                PrintWriter writer = new PrintWriter(socket.getOutputStream());

                //Write the theme to the new Client
                writer.write(theme + "\n");
                writer.flush();
                System.out.println("Wrote " + theme + ".");

                //Write all the options to the Server
                options.forEach(e ->  {
                    writer.write(e + "\n");
                    writer.flush();
                });

                //Add the new Client to the ArrayList of all Sockets.
                clients.add(socket);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


