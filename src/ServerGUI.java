import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.swing.*;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ServerGUI extends Application {

    ArrayList<String> options = new ArrayList<>();
    ObservableList<PieChart.Data> list = FXCollections.observableList(new ArrayList<PieChart.Data>());
    PrintWriter printWriter;
    String theme;
    @Override
    public void start(Stage stage)
    {

        BorderPane pane = new BorderPane();

        VBox vBox = new VBox();
        //[

        HBox themeBox = new HBox();
        //{
        Label themelbl = new Label("Theme: ");
        TextField themeField = new TextField();
        Button startPoll = new Button("Start Poll");
        //}


        HBox optionHBox = new HBox();
        //{
        ComboBox optionCheckBox = new ComboBox();
        Label optionlbl = new Label("Options: ");
        Button addOption = new Button("+");
        Button removeOption = new Button("-");
        //}

        PieChart pieChart = new PieChart();

        HBox saveBox = new HBox();
        //{
        Button updateData = new Button("Update");
        Button saveTxT = new Button("Save as .txt");
        //}

        //]

        //Adds a new Item to the optionCheckBox.
        addOption.setOnAction(event -> {

            String s;

            Optional<String > result = input("Options name", "New Option",
                    null, "Please Enter your Option name: ");

            if (result.isPresent()) {
                s = result.get();
                options.add(s);
                optionCheckBox.getItems().add(s);
                optionCheckBox.setValue(s);
            }

        });

        removeOption.setOnAction(event -> {
            optionCheckBox.getItems().remove(optionCheckBox.getSelectionModel().getSelectedIndex());
            options.remove(optionCheckBox.getSelectionModel().getSelectedItem().toString());
        });

        saveTxT.setOnAction(event -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File("/home/me/Documents"));
            int retrival = chooser.showSaveDialog(null);
            if (retrival == JFileChooser.APPROVE_OPTION) {
                File file = chooser.getSelectedFile();
                boolean okay = true;
                try {
                    if(file.toString().substring(file.toString().length() - 4, file.toString().length()).equals(".txt")) {
                        if (file.exists()) {
                            okay = okay("Override?", "File already exists", "Do you want to override the File?");

                        }
                    }
                    else {
                        if (new File(file + ".txt").exists()) {
                            okay = okay("Override?", "File already exists", "Do you want to override the File?");
                        }
                    }
                    if(okay) {
                        printWriter = new PrintWriter(file, "UTF-8");
                        if (file.toString().substring(file.toString().length() - 4, file.toString().length()).equals(".txt"))
                            printWriter = new PrintWriter(chooser.getSelectedFile(), "UTF-8");


                        Handler.update().forEach((str, i) -> {
                            printWriter.println(str + ": " + i);
                        });
                        printWriter.close();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });


        updateData.setOnAction(event -> {
            HashMap<String, Integer> optionCount = Handler.update();
            list.clear();
            for(Map.Entry<String, Integer> entry : optionCount.entrySet()) {
                list.add(new PieChart.Data(entry.getKey(), entry.getValue()));
            }
            list.forEach(data ->
                    data.nameProperty().bind(
                            Bindings.concat(data.getName(), ": ", data.pieValueProperty().intValue())
                    )
            );
            vBox.getChildren().clear();
            vBox.getChildren().addAll(new Label("Theme: " + theme), new PieChart(list), saveBox);
        });


        //Starting a server
        startPoll.setOnAction(event -> {
            new Thread(new Server(themeField.getText(), options)).start();
            theme = themeField.getText();
        });


        themeBox.setSpacing(20);
        themeBox.setMargin(themelbl, new Insets(0, 0, 0, 10));

        optionHBox.setSpacing(20);
        optionHBox.setMargin(optionlbl, new Insets( 0, -5, 0, 10));

        saveBox.setSpacing(20);


        vBox.setSpacing(5);

        vBox.getChildren().addAll(themeBox, optionHBox);

        themeBox.getChildren().addAll(themelbl, themeField, startPoll);
        optionHBox.getChildren().addAll(optionlbl, optionCheckBox, addOption);
        saveBox.getChildren().addAll(updateData, saveTxT);

        pane.setTop(vBox);
        pane.setCenter(pieChart);
        pane.setBottom(saveBox);

        Scene scene = new Scene(pane, 500, 600);
        stage.setScene(scene);
        stage.show();

    }


    //Creates a TextInputDialog for making Input available.
    private Optional<String> input(String defaultValue, String title, String headerText, String contentText) {
        TextInputDialog dialog = new TextInputDialog(defaultValue);
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
        dialog.setContentText(contentText);
        return dialog.showAndWait();
    }
    boolean okay(String title, String headerText, String contentText){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            return true;
        } else {
            return false;
        }
    }

    public static void start() {
        launch();
    }
}
